﻿namespace IP_Airport
{
    public interface IFlyingTransport
    {
        void Fly(string origin, string destination, int passengers);
    }
}
