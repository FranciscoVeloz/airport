﻿using System;

namespace IP_Airport
{
    public class Airplane : IFlyingTransport
    {
        public void Fly(string origin, string destination, int passengers)
        {
            Console.WriteLine($"El vuelo en avion de origen: {origin} con destino: {destination} lleva {passengers} pasajeros");
        }
    }
}
