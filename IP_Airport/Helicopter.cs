﻿using System;

namespace IP_Airport
{
    public class Helicopter : IFlyingTransport
    {
        public void Fly(string origin, string destination, int passengers)
        {
            Console.WriteLine($"El vuelo en Helicoptero de origen: {origin} con destino: {destination} lleva {passengers} pasajeros");
        }
    }
}
